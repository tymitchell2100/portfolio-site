import React, { Component } from 'react'

class Timeline extends Component {
    render() {
        return (
            <div className="timeline">
                <div className="container left">
                    <div className="content">
                        <h2 className="experience-title">Xtern Bootcamp</h2>
                        <p className="experience-dates">May 13 - June 2, 2018</p>
                    </div>
                </div>
                <div className="container right">
                    <div className="content">
                        <h2 className="experience-title">Kenzie Academy</h2>
                        <p className="experience-dates">since October 2018</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Timeline
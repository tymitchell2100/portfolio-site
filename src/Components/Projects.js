import React, { Component } from 'react'
import projectsObject from '../projects'
import Project from './Project'

class Projects extends Component {
    constructor() {
        super()

        this.projects = projectsObject.map((project, index) => 
            <Project key={index} name={project.name} url={project.url} srcPath={project.src} />
        )
    }

    render() {
        return (
            <div id="projects">
                <div id="projects-anchor"></div>
                <h1>Projects</h1>
                <div id='projects-container'>
                    {this.projects}
                </div>
            </div>
        )
    }
}

export default Projects
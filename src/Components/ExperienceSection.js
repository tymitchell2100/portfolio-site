import React, { Component } from 'react'
import Timeline from './Timeline'

class ExperienceSection extends Component {
    render() {
        return (
            <div id="experience-section">
                <div id='experience-anchor'></div>
                <h1>Experience</h1>
                <Timeline />
            </div>
        )
    }
}

export default ExperienceSection
import React, { Component } from 'react'

class Contact extends Component {
    render() {
        return (
            <div id='contact'>
                <h1>Contact Me</h1>
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }} id='contact'>
                    <a href="https://www.linkedin.com/in/tyler-ward-09928a113/">
                        <i class="social fab fa-linkedin"></i>
                    </a>
                    <a href="https://github.com/tymitchell21">
                        <i class="social fab fa-github-alt"></i>
                    </a>
                    <a href="https://gitlab.com/tymitchell2100">
                        <i class="social fab fa-gitlab"></i>
                    </a>
                    <a href="mailto: tymitchellw@protonmail.com">
                        <i class="social fas fa-envelope"></i>
                    </a>
                </div>
            </div>
        )
    }
}

export default Contact
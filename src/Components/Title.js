import React, { Component } from 'react'

class Title extends Component {
  render() {
    return (
        <div id="title">
            <h1 id="site-title">
                <a
                  id="logo"
                  className="logo"
                  href="https://tymitchell2100.gitlab.io/portfolio-site/"
                >Life. by Tyler</a>
            </h1>
        </div>
    )
  }
}

export default Title
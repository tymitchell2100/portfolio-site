import React, { Component } from 'react'

class Project extends Component {
    render() {
        return (
            <a className="project" href={this.props.url}>
                <img className="project-img" alt='' src={this.props.srcPath} />
                <div className="description">
                    <h1 className='project-description'>{this.props.name}</h1>
                </div>
            </a>
        )
    }
}

export default Project
import React, { Component } from 'react'
import About from './About'
import Projects from './Projects'
import Contact from './Contact'
import ExperienceSection from './ExperienceSection'

class MainContainer extends Component {
    render() {
        return (
            <div id="main-container">
                <About />
                <div className='divider'></div>
                <Projects />
                <div className='divider'></div>
                <ExperienceSection />
                <div className='divider'></div>
                <Contact />
            </div>
        )
    }
}

export default MainContainer
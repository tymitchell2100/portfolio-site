import React, { Component } from 'react'

class About extends Component {
    render() {
        return (
            <div id="about-section" className="wrap">
                <img src={'./img/ProfileImageFinal.jpg'} id='self-photo' alt=""/>
                <div id="about-text">
                    <h1>A little about myself</h1>
                    <p>
                        Hi! I am an eager and motivated sprouting Software Engineer,
                        seeking to develop my skills anyway that I can. My interests outside of
                        development include economics, history, investing, the outdoors and all
                        aspects of technology.
                    </p>
                </div>
            </div>
        )
    }
}

export default About
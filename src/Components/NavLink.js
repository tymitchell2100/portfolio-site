import React, { Component } from 'react'

class NavLink extends Component {
    render() {
        return (
            <li className="menu-item">
                <a href={this.props.link}>{this.props.name}</a>
            </li>
        )
    }
}

export default NavLink
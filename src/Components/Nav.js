import React, { Component } from 'react'
import NavLink from './NavLink'

class Nav extends Component {
    constructor() {
        super()

        this.navs = [
            {
                name: 'about',
                link: '#main-container'
            },
            {
                name: 'projects',
                link: '#projects-anchor'
            },
            {
                name: 'experience',
                link: '#experience-anchor'
            },
            {
                name: 'contact',
                link: '#contact'
            }
        ]
    }

    returnNavLinks() {
        return this.navs.map((nav,index) => {
            return <NavLink key={index} link={nav.link} name={nav.name} />
        })
    }

    render() {
        return (
            <nav id="nav-primary">
                <div className="wrap">
                    <ul id="nav">
                        {this.returnNavLinks()}
                    </ul>
                </div>
            </nav>
        );
  }
}

export default Nav
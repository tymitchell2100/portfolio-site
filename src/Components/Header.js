import React, { Component } from 'react';
import Title from './Title'
import Nav from './Nav'

class Header extends Component {
  render() {
    return (
        <header id="header">
            <div className="wrap">
                <Title />
                <Nav />
            </div>
        </header>
    );
  }
}

export default Header;
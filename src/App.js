import React, { Component } from 'react';
import Header from './Components/Header'
import MainContainer from './Components/MainContainer'
import './App.css';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <div className='divider'></div>
        <MainContainer />
      </React.Fragment>
    );
  }
}

export default App;

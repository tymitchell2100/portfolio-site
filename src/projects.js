const projectsObject = [
    {
        name: 'Kwitter (Twitter clone)',
        url: 'https://nmdanner123.gitlab.io/assessment---kwitter-frontend/',
        src: './img/kwitter.png'
    },
    {
        name: 'Simple Todo App',
        url: 'https://tymitchell2100.gitlab.io/assessment---todo-app-part-3/',
        src: './img/todo.png'
    },
    {
        name: 'Simple Todo App',
        url: 'https://tymitchell2100.gitlab.io/assessment---react-photo-wall/',
        src: './img/photoWall.png'
    },
    {
        name: 'Favorite Scorsese Films',
        url: 'https://tymitchell21.github.io/Scorsese/',
        src: './img/scorsese.png'
    },
    {
        name: 'Jeopardy',
        url: 'https://tymitchell21.github.io/jeopardy-s_integrating/',
        src: './img/jeopardy.png'
    },
    {
        name: 'Combat Game',
        url: 'https://tymitchell21.github.io/combat-game/',
        src: './img/combat.png' 
    },
    {
        name: 'Minesweep',
        url: 'https://tymitchell21.github.io/mine-sweep/',
        src: './img/minesweep.png'
    },
    {
        name: 'Sokoban',
        url: 'https://tymitchell21.github.io/sokoban-1/',
        src: './img/sokoban.png'
    }
]

export default projectsObject